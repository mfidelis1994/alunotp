# Exercicios Técnicas de Programação (UNICSUL)

- [x] Crie um novo repositório local com um projeto à sua escolha e realize pelo menos dois commits neste repositório

- [x] Escolha um repositório disponível no gitlab.com e faça a clonagem do mesmo, identificando qual foi o autor do último commit realizado no projeto e a(s) linguagem(s) utilizadas.

### Git clone aleatório:

    https://gitlab.com/StuntsPT/FE2021.git

* O ultimo commit realizado foi do: Francisco Pina-Martins.
* A linguagem utilizada foi HTML.

- [x] Identifique a finalidade dos seguintes comandos:
```
    a) git init: cria um novo repositório.

    b) git config --global user.name "turing": faz a configuração do nome do usuário como "turing".

    c) git add EXERCICIO.txt: adiciona o arquivo EXERCICIO.txt na staging area.

    d) git add .: adiciona todos os arquivos do path na staging area pronto pra commit.

    e) git commit -m "Adicionado nova interface": salva mudanças no repositorio local e adiciona a mensagem de commit = 'Adicionado nova interface'.

    f) git commit: cria um commit, salvando as mudanças no repositorio local.

    g) git reset --hard HEAD: desfaz o ultimo commit de um repositorio.

    h) cd Downloads: abre a pasta Downloads pelo terminal.

    i) pwd: imprime o nome do diretorio local onde está aberto, no temrinal.

    j) cd ..: volta uma pasta de diretorio.

    k) ls: lista arquivos e diretorios no diretorio atual.

    l) git pull: trás as ultimas alterações feitas por outros desenvoldedores do projeto, fazendo o merge das alterações no codigo.

    m) git push: envia o conteudo de um repositorio local, para um repositorio remoto.

    n) git clone https://gitlab.com/rVenson/linguagemdeprogramacao: clona para o ambiente local os arquivos no endereço do repositorio.

    o) git diff: permite visualizar as alterações ainda não selecionadas para commit.

    p) git show: mostra detalhes do commit atual.

```

- [x] Descreva a função dos seguintes componentes do Git

    1) Stage Area, Commit: A stage area é uma area temporária pra salvar as modificações, ela permite que você continue fazendo alterações antes de realizar o commit.

    2) Local Repository: é o repositorio que está dentro da propria maquina. Geralmente iniciado na máquina ou clonado de algum repositorio remoto.

    3) Remote Repository: é o repositorio hospedado no serviço de versionamento do git.
    




